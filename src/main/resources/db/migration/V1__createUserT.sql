CREATE TABLE IF NOT EXISTS public.t_user
(
    id bigint NOT NULL,
    email text NOT NULL,
    password text NOT NULL,
    name text  NOT NULL,
    surname text NOT NULL,
    address text,
    phone text,
    CONSTRAINT t_user_pkey PRIMARY KEY (id),
    CONSTRAINT t_user_email_key UNIQUE (email)

)