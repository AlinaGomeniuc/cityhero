-- Table: public.event_table

-- DROP TABLE public.event_table;

CREATE TABLE public.event_table
(
    user_id integer,
    event_id integer NOT NULL,
    description text COLLATE pg_catalog."default" NOT NULL,
    photo bytea,
    address text COLLATE pg_catalog."default" NOT NULL,
    nrvotes integer NOT NULL,
    CONSTRAINT event_table_pkey PRIMARY KEY (event_id),
    CONSTRAINT event_table_user_id_fkey FOREIGN KEY (user_id)
        REFERENCES public.user_table (user_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.event_table
    OWNER to postgres;