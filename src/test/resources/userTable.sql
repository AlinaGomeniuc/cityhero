-- Table: public.user_table

-- DROP TABLE public.user_table;

CREATE TABLE public.user_table
(
    user_id integer NOT NULL,
    email text COLLATE pg_catalog."default" NOT NULL,
    password text COLLATE pg_catalog."default" NOT NULL,
    name text COLLATE pg_catalog."default" NOT NULL,
    surname text COLLATE pg_catalog."default" NOT NULL,
    address text COLLATE pg_catalog."default",
    phone text COLLATE pg_catalog."default",
    CONSTRAINT user_table_pkey PRIMARY KEY (user_id),
    CONSTRAINT user_table_email_key UNIQUE (email)

)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.user_table
    OWNER to postgres;