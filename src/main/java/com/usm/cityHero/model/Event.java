package com.usm.cityHero.model;

import com.google.common.base.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;
import javax.persistence.GenerationType;

@Entity
@Table(name = "t_event")
public class Event {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @Column(name = "description")
    private String description;

    @Column(name = "photo")
    private byte[] photo;

    @Column(name = "address")
    private String address;

    @Column(name = "number_of_votes")
    private int numberOfVotes;

    public Event(){

    }

    public Event(String description, byte[] photo, String address, int numberOfVotes) {
        this.description = description;
        this.photo = photo;
        this.address = address;
        this.numberOfVotes = numberOfVotes;
    }

    public User getUserId() {
        return user;
   }

    public Long getEventId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public String getAddress() {
        return address;
    }

    public int getNumberOfVotes() {
        return numberOfVotes;
    }

    public void setEventId(Long eventId) {
        this.id = eventId;
    }

    public void setUserId(User userId) { this.user = userId; }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setNumberOfVotes(int numberOfVotes) {
        this.numberOfVotes = numberOfVotes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Event event = (Event) o;
        return  id == event.id &&
                numberOfVotes == event.numberOfVotes &&
                Objects.equal(description, event.description) &&
                Objects.equal(photo, event.photo) &&
                Objects.equal(address, event.address);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id,description, photo, address, numberOfVotes);
    }
}
