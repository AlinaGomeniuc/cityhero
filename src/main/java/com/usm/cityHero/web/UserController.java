package com.usm.cityHero.web;

import com.usm.cityHero.dto.RegisterRequestDto;
import com.usm.cityHero.model.User;
import com.usm.cityHero.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.GetMapping;



@RestController
@RequestMapping("/user")
public class UserController {

    private final
    UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping(value = "/register")
    public String registerUser(@RequestBody RegisterRequestDto obj ) {
        userService.registerUser(obj);
        return "ok"; //TODO  for a proper ResponseEntity
        }


    @GetMapping(value = "/email")
    public User user(@RequestParam String email) {
        return userService.getUserByEmail(email);
    }


}
