-- Table: public.vote_table

-- DROP TABLE public.vote_table;

CREATE TABLE public.vote_table
(
    user_id integer,
    event_id integer,
    CONSTRAINT vote_table_event_id_fkey FOREIGN KEY (event_id)
        REFERENCES public.event_table (event_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    CONSTRAINT vote_table_user_id_fkey FOREIGN KEY (user_id)
        REFERENCES public.user_table (user_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.vote_table
    OWNER to postgres;