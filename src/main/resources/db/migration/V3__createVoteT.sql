CREATE TABLE IF NOT EXISTS public.t_vote
(
    id bigint,
    user_id bigint,
    event_id bigint,
    CONSTRAINT t_vote_pkey PRIMARY KEY (id),
    CONSTRAINT t_vote_event_id_fkey FOREIGN KEY (event_id)
        REFERENCES public.t_event (id) ,
	CONSTRAINT t_vote_user_id_fkey FOREIGN KEY (user_id)
        REFERENCES public.t_user (id)
)