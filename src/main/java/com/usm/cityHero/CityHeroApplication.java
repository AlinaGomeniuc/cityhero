package com.usm.cityHero;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.jpa.JpaRepositoriesAutoConfiguration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories("com.usm.cityHero.dao")
@SpringBootApplication(exclude = JpaRepositoriesAutoConfiguration.class)
public class CityHeroApplication {

	public static void main(String[] args) {
		SpringApplication.run(CityHeroApplication.class, args);
	}
}
