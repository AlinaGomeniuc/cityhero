package com.usm.cityHero.service.impl;

import com.usm.cityHero.dao.UserDao;
import com.usm.cityHero.dto.RegisterRequestDto;
import com.usm.cityHero.model.User;
import com.usm.cityHero.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private final
    UserDao userDao;

    @Autowired
    public UserServiceImpl(UserDao userDao) {
        this.userDao = userDao;
    }

    public void registerUser(RegisterRequestDto registerRequest){
       User user = new User(registerRequest.getEmail(),
                            registerRequest.getPassword(),
                            registerRequest.getName(),
                            registerRequest.getSurname(),
                            registerRequest.getAddress(),
                            registerRequest.getPhone());
       userDao.save(user);
    }

    public User getUserByEmail(String email) {
        Optional<User> user = userDao.findByEmail(email);
        if (user.isPresent())
            return user.get();
        return null; //TODO for adding business validation
    }

}
