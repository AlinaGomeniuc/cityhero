CREATE TABLE IF NOT EXISTS public.t_event
(   id bigint NOT NULL,
    user_id bigint,
    description text NOT NULL,
    photo bytea,
    address text  NOT NULL,
    number_of_votes integer NOT NULL,
    CONSTRAINT t_event_pkey PRIMARY KEY (id),
    CONSTRAINT t_event_user_id_fkey FOREIGN KEY (user_id)
        REFERENCES public.t_user (id)
)