package com.usm.cityHero.service;

import com.usm.cityHero.dto.RegisterRequestDto;
import com.usm.cityHero.model.User;

public interface UserService {

    void registerUser(RegisterRequestDto obj);
    User getUserByEmail(String email);
}
